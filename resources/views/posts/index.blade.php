@extends('layouts.app')

@section('content')
    <h1>
        All the Posts
        {{ link_to_route("posts.create", "New", null, ['class' => "btn btn-success btn-sm"]) }}
    </h1>

    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif

    <div id="posts"></div>
@endsection

@push('scripts')
<script>
    ReactDOM.render(React.createElement(PostsController.list(), { url: '{{ route("posts.index") }}' }),
        document.getElementById('posts')
    );
</script>
@endpush