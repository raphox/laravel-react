@extends('layouts.app')

@section('content')
    <h1>New Post</h1>

    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif

    {!! Form::open(array('route' => ['posts.store'])) !!}
        @include('posts._form')
    {!! Form::close() !!}
@endsection