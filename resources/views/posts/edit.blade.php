@extends('layouts.app')

@section('content')
    <h1>Edit Post</h1>

    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif

    {!! Form::model($post, ['method' => 'PATCH', 'route' => ['posts.update',$post->id]]) !!}
        @include('posts._form')
    {!! Form::close() !!}
@endsection