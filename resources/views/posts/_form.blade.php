<div id="post"></div>

@push('scripts')
@php($data = collect(count(old()) ? old() : $post ))
<script>
    ReactDOM.render(React.createElement(PostsController.form(), {
            url: '{{ route("posts.index") }}',
            data: {!! $data->isNotEmpty() ? $data->toJson() : '{}' !!},
            errors: {!! $errors->isNotEmpty() ? $errors->toJson() : '{}' !!}
        }),
        document.getElementById('post')
    );
</script>
@endpush