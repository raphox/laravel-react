import ComponentForm from './Form';
import yup from 'yup';
import { Col, Button, Form, FormGroup, Label, Input, FormText, FormFeedback } from 'reactstrap';

export default class PostForm extends ComponentForm {
  constructor(props) {
    super(props);

    const schema = yup.object().shape({
      title: yup.string().required(),
      description: yup.string().required()
    });

    this.state = {
      schema: schema,
      data: { title: '', description: '', ...props.data },
      errors: { ...props.errors }
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
  }

  render() {
    return (
      <div>
        <FormGroup row>
          <Label for="title" sm={2}>Title</Label>
          <Col sm={10}>
            <Input type="text" name="title" id="title"
              invalid={_.get(this.state.errors, 'title')}
              value={this.state.data.title}
              onChange={this.handleChange} />
            <FormFeedback>{_.get(this.state.errors, 'title')}</FormFeedback>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="description" sm={2}>Description</Label>
          <Col sm={10}>
            <Input type="textarea" name="description" id="pdescription"
              invalid={_.get(this.state.errors, 'description')}
              value={this.state.data.description}
              onChange={this.handleChange} />
            <FormFeedback>{_.get(this.state.errors, 'description')}</FormFeedback>
          </Col>
        </FormGroup>
        <FormGroup check row>
          <Col sm={{ size: 10, offset: 2 }} className="p-0">
            <Button type="submit" onClick={this.handleSubmit}>Submit</Button>{' '}
            { this.state.id ? (
              <Button type="button" onClick={this.handleCancel} color="danger">Cancel</Button>
            ) : (
              <Button type="button" onClick={this.handleCancel} color="info">Back</Button>
            )}
          </Col>
        </FormGroup>
      </div>
    );
  }
}