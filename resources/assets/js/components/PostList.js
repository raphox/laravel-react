import React, { Component } from 'react';

import axios from 'axios';
import { Button } from 'reactstrap';
import ReactTable from 'react-table';
import "react-table/react-table.css";

const requestData = (pageSize, page, sorted, filtered) => {
    return new Promise((resolve, reject) => {
        axios.get('/posts', {
            params: {
                pageSize: pageSize,
                page: page,
                sorted: sorted,
                filtered: filtered
            }
        }).then(function (response) {
            const data = response.data;

            // You must return an object containing the rows of the current page, and optionally the total pages number.
            const res = {
                rows: data,
                pages: response.headers['x-data-total-pages']
            };

            resolve(res);
        }).catch(function (error) {
            console.log(error);
        });
    });
};

export default class PostsList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: [],
            pages: null,
            loading: true
        };

        this.fetchData = this.fetchData.bind(this);
    }

    fetchData(state, instance) {
        // Whenever the table model changes, or the user sorts or changes pages, this method gets called and passed the current table model.
        // You can set the `loading` prop of the table to true to use the built-in one or show you're own loading bar if you want.
        this.setState({ ...state, loading: true });

        // Request the data however you want.  Here, we'll use our mocked service we created earlier
        requestData(
            state.pageSize,
            state.page,
            state.sorted,
            state.filtered
        ).then(res => {
            // Now just get the rows of data to your React Table (and update anything else like total pages or loading)
            this.setState({
                data: res.rows,
                pages: res.pages,
                loading: false
            });
        });
    }

    handleEdit(row) {
        const data = row.original;

        window.location = `${this.props.url}/${data.id}/edit`;
    }

    handleDelete(row) {
        const data = row.original;

        axios.delete(`${this.props.url}/${data.id}`).then((response) => {
            this.fetchData(this.state);
        }).catch(function (error) {
            console.log(error);
        });
    }

    render() {
        const { data, pages, loading } = this.state;

        return (
            <div>
                <ReactTable
                    columns={[{
                        Header: 'Title',
                        accessor: 'title' // String-based value accessors!
                    }, {
                        Header: 'Description',
                        accessor: 'description'
                    }, {
                        Header: 'Actions',
                        filterable: false,
                        sortable: false,
                        maxWidth: 150,
                        className: 'text-center',
                        Cell: row => (
                            <div>
                                <Button onClick={() => this.handleEdit(row)} color="info" size="sm">Edit</Button>{' '}
                                <Button onClick={() => this.handleDelete(row)} color="danger" size="sm">Delete</Button>
                            </div>
                        )
                    }]}
                    manual // Forces table not to paginate or sort automatically, so we can handle it server-side
                    data={data}
                    pages={pages} // Display the total number of pages
                    loading={loading} // Display the loading overlay when we need it
                    onFetchData={this.fetchData} // Request new data when things change
                    filterable
                    defaultPageSize={10}
                    className="-striped -highlight"
                />
            </div>
        );
    }
}