import React, { Component } from 'react';

import yup from 'yup';

export default class Form extends Component {
  handleChange(event) {
    const target = event.target;
    const path = target.name;
    let data = { ...this.state.data };

    _.set(data, path, target.value);

    this.setState({ data: data }, () => this.validate(data, path));
  }

  handleSubmit(event) {
    const target = event.target;

    event.preventDefault();
    
    this.validate(this.state.data)
      .then((valid) => valid && target.form.submit());
  }

  handleCancel(event) {
    window.location = this.props.url;
  }

  validate(data, path) {
    let schema;
    let errors = { ...this.state.errors };

    if (path) {
      schema = yup.reach(this.state.schema, path);
      data   = _.get(data, path);
    } else {
      schema = this.state.schema;
    }

    if (path) _.set(errors, path, null);
    else errors = {};

    return schema.validate(data, { abortEarly: false })
      .catch((exception) => {
        exception.inner.forEach(error => {
          const _path = path || error.path;

          _.set(errors, _path, error.errors.map((item) => item.replace(_path, 'this')));
          this.setState({ errors: errors });
        });
      });
  }
}