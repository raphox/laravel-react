<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pageSize = $request->input('pageSize', 10);
        $page = $request->input('page', 0);
        $sorted = $request->input('sorted', []);
        $filtered = $request->input('filtered', []);

        // get all the posts
        $posts = \DB::table('posts');

        foreach($filtered as $filter) {
            $data = json_decode($filter, true);
            $posts->where($data['id'], 'like', "%{$data['value']}%");
        }

        foreach($sorted as $sorter) {
            $data = json_decode($sorter, true);
            $posts->orderBy($data['id'], $data['desc'] == true ? 'desc' : 'asc');
        }

        $pager = with(clone $posts)
            ->offset($page * $pageSize)
            ->limit($pageSize);

        // load the view and pass the posts
        if ($request->ajax() || $request->expectsJson()) {
            return response()
                ->json($pager->get())
                ->header('X-Data-Total', Post::count())
                ->header('X-Data-Total-Filtered', $posts->count())
                ->header('X-Data-Total-Pages', ceil($posts->count() / $pageSize));
        } else {
            return view('posts.index', array(
                'posts' => $pager->get()
            ));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create', array(
            'post' => new Post()
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\PostRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        // store
        $nerd = Post::create($request->input());

        // redirect
        return redirect('posts')->with('message', 'Successfully created the post!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        return view('posts.edit', array(
            'post' => $post
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\PostRequest $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, Post $post)
    {
        if($post->update($request->input())){
            \Session::flash('message', 'Successfully updated the post!');
        }
        else{
            \Session::flash('message', 'Error on updated the post!');
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Post $post)
    {
        // delete
        $post->delete();

        // load the view and pass the posts
        if ($request->ajax() || $request->expectsJson()) {
            return response()->json([
                'success' => true
            ]);
        } else {
            // redirect
            return redirect('posts')->with('message', 'Successfully deleted the post!');
        }
    }
}
