Projeto com exemplo de integração Laravel 5.6 + Bootstrap 4 + React

## Gerando um projeto do zero

1. Fazer download do Composer

    ```
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
    php composer-setup.php
    php -r "unlink('composer-setup.php');"
    # Reference https://getcomposer.org/download/
    ```

2. Criar novo projeto com Laravel

    ```
    php composer.phar create-project --prefer-dist laravel/laravel laravel-react
    cd laravel-react
    ```

3. Alterar lib de interface do Vue para o React:

    ```
    php artisan preset react
    npm install && npm run dev
    ```

4. Ferramentas para facilitar o desenvolvimento:

    1. PHP:

        * Laravel Collective (https://laravelcollective.com): `php ../composer.phar require "laravelcollective/html":"^5.4.0"`
        * Laravel Debugbar https://github.com/barryvdh/laravel-debugbar): `php ../composer.phar require barryvdh/laravel-debugbar --dev`
    
    2. Javascript (NPM):

        * Reactstrap (https://reactstrap.github.io/): `npm install --save-dev reactstrap@next`
        * React-table (https://react-table.js.org): `npm install --save-dev react-table`

        > **Atenção:** Veja que estamos utilizando `--save-dev`, isso porque não vamos precisar dele no ambiente de produção, já que todo o código será "*compilado*" e armazenado como *Javascript* puro no final.

## Gerando um CRUD

1. Gerar modelo migration

    ```
    php artisan make:model Post -m
    # ... Acesse o arquivo database/migrations/2018_02_20_110130_create_posts_table.php e
    # insira as colunas necessárias seguindo a documentação https://laravel.com/docs/5.6/migrations
    # Depois gere o controller para administrar seus objetos
    php artisan make:controller -m Post Posts --resource
    ```

2. Gerar resource controller (CRUD) para administrar seus objetos

    ```
    php artisan make:controller -m Post PostsController --resource
    ```

    Altere o arquivo `routes/web.php` e adicione a referência ao novo controller:

    ```
    Route::resource('posts', 'PostController');
    ```

3. Gerar views

    ```
    mkdir -p resources/views/posts
    touch resources/views/posts/index.blade.php resources/views/posts/create.blade.php resources/views/posts/show.blade.php resources/views/posts/edit.blade.php
    ```

Essa é a base que vamos utilizar, onde utilizamos o CLI (Artisan) do Laravel. Agora que temos grande parte dos arquivos gerados, será mais fácil você verificar o código para ver como ficou a integração Laravel+React.